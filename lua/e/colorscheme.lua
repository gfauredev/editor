local colorscheme = "gruvbox-baby"

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end

pcall(vim.cmd, "highlight Normal ctermbg=NONE guibg=NONE")
