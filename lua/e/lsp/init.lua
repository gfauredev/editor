local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require "e.lsp.lsp-installer"
require("e.lsp.handlers").setup()
require "e.lsp.null-ls"
