local opt = {noremap = true, silent = true}

local map = vim.api.nvim_set_keymap -- shorter function name

map("", "<Space>", "<Nop>", opt) -- space as leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes reminder
--   normal mode       = n
--   insert mode       = i
--   visual mode       = v
--   visual block mode = x
--   term mode         = t
--   command mode      = c
--   operator pending  = o

-- movement
map("n", "c", "h", opt)
--map("n", "t", "j", opt)
--map("n", "s", "k", opt)
map("n", "t", "gj", opt)
map("n", "s", "gk", opt)
map("n", "r", "l", opt)
map("v", "c", "h", opt)
map("v", "t", "j", opt)
map("v", "s", "k", opt)
map("v", "r", "l", opt)
map("x", "c", "h", opt)
map("x", "t", "j", opt)
map("x", "s", "k", opt)
map("x", "r", "l", opt)
map("o", "c", "h", opt)
map("o", "t", "j", opt)
map("o", "s", "k", opt)
map("o", "r", "l", opt)
---- movement g as default
map("n", "gt", "j", opt)
map("n", "gs", "k", opt)
---- MAJ move for fast move
map("n", "C", "0", opt)
map("n", "T", "<C-d>", opt)
map("n", "S", "<C-u>", opt)
map("n", "R", "$", opt)
map("v", "C", "0", opt)
map("v", "T", "<C-d>", opt)
map("v", "S", "<C-u>", opt)
map("v", "R", "$", opt)
map("x", "C", "0", opt)
map("x", "T", "<C-d>", opt)
map("x", "S", "<C-u>", opt)
map("x", "R", "$", opt)
map("o", "C", "0", opt)
map("o", "T", "<C-d>", opt)
map("o", "S", "<C-u>", opt)
map("o", "R", "$", opt)
---- words moving
map("n", "é", "w", opt)
map("n", "É", "W", opt)
map("v", "é", "w", opt)
map("v", "É", "W", opt)
map("x", "é", "w", opt)
map("x", "É", "W", opt)
---- till
map("n", "h", "t", opt)
map("n", "H", "T", opt)
map("v", "h", "t", opt)
map("v", "H", "T", opt)
map("x", "h", "t", opt)
map("x", "H", "T", opt)
------ till and to forward and backward
map("n", ";", ",", opt)
map("n", ",", ";", opt)
map("v", ";", ",", opt)
map("v", ",", ";", opt)
map("x", ";", ",", opt)
map("x", ",", ";", opt)
-- Navigate buffers
map("n", "<a-r>", ":bnext<CR>", opt)
map("n", "<a-c>", ":bprevious<CR>", opt)

-- replace
map("n", "j", "r", opt)
map("n", "J", "R", opt)
map("n", "l", "c", opt)
map("n", "L", "C", opt)
map("v", "j", "r", opt)
map("v", "J", "R", opt)
map("v", "l", "c", opt)
map("v", "L", "C", opt)
map("x", "j", "r", opt)
map("x", "J", "R", opt)
map("x", "l", "c", opt)
map("x", "L", "C", opt)
map("o", "j", "r", opt)
map("o", "J", "R", opt)
map("o", "l", "c", opt)
map("o", "L", "C", opt)

-- go to cmd line mode
map("n", "’", ":", opt)
map("n", "'", ":", opt)

-- changing vim window
map("n", "w", "<C-w>", opt)
map("n", "W", "<C-w><C-w>", opt)
map("n", "wc", "<C-w>h", opt)
map("n", "wt", "<C-w>j", opt)
map("n", "ws", "<C-w>k", opt)
map("n", "wr", "<C-w>l", opt)

-- Indenting more accessible
map("n", "»", ":><CR>", opt)
map("n", "«", ":<<CR>", opt)
map("v", "»", ":><CR>", opt)
map("v", "«", ":<<CR>", opt)
map("x", "»", ":><CR>", opt)
map("x", "«", ":<<CR>", opt)

-- special
map("n", "gr", "<C-r>", opt)
map("n", "<leader>gf", ":GrammarousCheck --lang=fr<CR>", opt)
map("n", "<leader>ge", ":GrammarousCheck --lang=en-US<CR>", opt)

-- Clear highlights
map("n", "<leader>h", "<cmd>nohlsearch<CR>", opt)

-- Close buffers
map("n", "<S-q>", "<cmd>Bdelete!<CR>", opt)

-- Better paste
map("v", "p", '"_dP', opt)

-- NvimTree
map("n", "<leader>e", ":NvimTreeToggle<CR>", opt)

-- Telescope
map("n", "<leader>ff", ":Telescope find_files<CR>", opt)
map("n", "<leader>ft", ":Telescope live_grep<CR>", opt)
map("n", "<leader>fp", ":Telescope projects<CR>", opt)
map("n", "<leader>fb", ":Telescope buffers<CR>", opt)

-- Git
map("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opt)

-- Comment
map("n", "<leader>/", "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", opt)
map("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle_linewise_op(vim.fn.visualmode())<CR>', opt)

-- DAP
map("n", "<leader>db", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", opt)
map("n", "<leader>dc", "<cmd>lua require'dap'.continue()<cr>", opt)
map("n", "<leader>di", "<cmd>lua require'dap'.step_into()<cr>", opt)
map("n", "<leader>do", "<cmd>lua require'dap'.step_over()<cr>", opt)
map("n", "<leader>dO", "<cmd>lua require'dap'.step_out()<cr>", opt)
map("n", "<leader>dr", "<cmd>lua require'dap'.repl.toggle()<cr>", opt)
map("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>", opt)
map("n", "<leader>du", "<cmd>lua require'dapui'.toggle()<cr>", opt)
map("n", "<leader>dt", "<cmd>lua require'dap'.terminate()<cr>", opt)
